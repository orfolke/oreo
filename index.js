const Discord = require('discord.js');
const client = new Discord.Client();
const config = require("./config.json");
const Faker = require('faker');
var isReady = true;
var prefix = "!"

//========================================================================================

client.on('ready', () => {
  console.log(`${client.user.tag} wjezdza na rejony kek`);
  client.user.setStatus("online")
  client.user.setActivity('Netflix', { type: 'WATCHING' })
});

client.on("message", (message) => {
    if (!message.content.startsWith(prefix)) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    var text = args.slice(2).join(" ");
    var text2 = args.slice(1).join(" ");
    const command = args.shift().toLowerCase();

    //========================================================================================

    if(command == "rozlacz") {
        if(message.author.id == config.id) {
            message.channel.send("Rozłączanie...");
            client.user.setStatus('invisible');
            client.destroy();
        }
    }

    //========================================================================================

    if(command == "awatar") {
        if(args[0] == null) {
            message.channel.sendMessage({
              "embed": {
                title: "Oto Twój awatar:",
                "image": {
                  "url": message.author.avatarURL,
                }
              }
            });

        } else {
            message.guild.members.forEach(function(member) {
                if(`<@!${member.user.id}>` == args[0]) {
                    message.channel.sendMessage({
                      "embed": {
                        title: "Oto awatar użytkownika " + member.user.username,
                        "image": {
                          "url": member.user.avatarURL,
                        }
                      }
                    })
                }
            });
        }
    }

    //========================================================================================

    if(command == "zmien-awatar") {
        if(message.author.id == config.id) {
            if(args[0] == null) {
                if(Array.from(message.attachments)[0] == null) {
                    message.reply("Podaj link do awatara!")
                    .then(user => message.channel.send("Zmieniono awatar!"))
                    .catch(console.error);
                } else {
                    message.attachments.forEach(function(a) {
                        client.user.setAvatar(a.url)
                        .then(user => message.channel.send("Zmieniono awatar!"))
                        .catch(console.error);
                    });
                }
            } else {
                client.user.setAvatar(args[0])
                .then(user => message.channel.send("Zmieniono awatar!"))
                .catch(console.error);
            }
        }
    }

    //========================================================================================

    if(command == "zmien-nick") {
        if(message.author.id == config.id) {
            client.user.setUsername(text2).then(message.react("✅"));
        }
    }

    //========================================================================================

    if(command == "wyrzuc") {
        if(message.guild.member(message.author).hasPermission("KICK_MEMBERS") == true) {
            if(args[0] == null) {
                message.reply("Podaj użytkownika do wyrzucenia!");
                message.react("❌");
            } else {
                message.guild.members.forEach(function(member) {
                    if(`<@${member.user.id}>` == args[0]) {
                        if (member.kickable == true) {
                            member.kick("Wyrzucono przez użytkownika: " + message.author.tag);
                            message.channel.send("Wyrzucono użytkownika: " + member.user.username);
                            message.react("✅");
                        } else {
                            message.channel.send("Bot nie ma uprawnień do wyrzucenia tego użytkownika!");
                            message.react("❌");
                        }
                    }
                });
            }
        } else {
            message.reply("Brak uprawnień!");
            message.react("❌");
        }
    }

    //========================================================================================

    if(command == "zbanuj") {
        if(message.guild.member(message.author).hasPermission("BAN_MEMBERS") == true) {
            if(args[0] == null) {
                message.reply("Podaj użytkownika do zbanowania!");
                message.react("❌");
            } else {
                message.guild.members.forEach(function(member) {
                    if(`<@${member.user.id}>` == args[0]) {
                        if(member.bannable == true) {
                            member.ban("Zbanowano przez użytkownika: " + message.author.tag);
                            message.channel.send("Zbanowano użytkownika: " + member.user.username);
                            message.react("✅");
                        } else {
                            message.channel.send("Bot nie ma uprawnień do zbanowania tego użytkownika!");
                            message.react("❌");
                        }
                    }
                });
            }
        } else {
            message.reply("Brak uprawnień!");
            message.react("❌");
        }
    }

    //========================================================================================

    if(command == "serwer-nazwa") {
           if(message.guild.member(message.author).hasPermission("MANAGE_GUILD") == true) {
               if(args[0] == null) {
                   message.reply("Podaj nazwę serwera!");
               } else {
                   message.guild.setName(text2, "Zmieniono przez użytkownika: " + message.author.tag);
                   message.react("✅");
               }
           }
       }

       //========================================================================================

      if(command == "admin") {
        if(message.guild.member(message.author).hasPermission("ADMINISTRATOR") == true) {
          message.channel.send("Posiadasz uprawnienia administratorskie.")
        }
        if(message.guild.member(message.author).hasPermission("ADMINISTRATOR") == false) {
          message.channel.send("Nie posiadasz uprawnień administratorskich.")
        }
      }

      //========================================================================================

    if (command === "help") {
        message.channel.send(":mailbox_with_mail: | Sprawdź wiadomości prywatne!")
        message.author.send({embed: {
            color: 3447003,
            author: {
                name: client.user.username,
                icon_url: client.user.avatarURL
            },
            fields: [{
                name: "!status",
                value: "Zmienia status\n" + "Opcje użycia:\n" + "online | dnd | idle | invisible"
            },
            {
                name: "!ciastko",
                value: "Odtwarza dźwięk jedzenia ciastka na kanale głosowym"
            },
            {
                name: "!awatar",
                value: "Pokazuje Twój awatar/Pokazuje awatar wzmienionej osoby"
            },
            {
                name: "!wyrzuc",
                value: "Wyrzuca z serwera wzmienioną osobę"
            },
            {
                name: "!zbanuj",
                value: "Banuje z serwera wzmienioną osobę"
            },
            {
              name: "!serwer-nazwa",
              value: "Zmienia nazwe serwera na którym została wpisana komenda (wymaga uprawnień)"
            },
            {
              name: "!przytul",
              value: "Przytula wzmienioną osobę"
            },
            {
              name: "!hardbass",
              value: "Losuje link do hardbassu"
            },
            {
              name: "!google",
              value: "Googluje fraze (!google <fraza>)"
            },
            {
              name: "!strony",
              value: "Lista fajnych stron WWW"
            },
            {
              name: "!uzytkownik",
              value: "Informacje o użytkowniku"
            },
            {
              name: "!github",
              value: "Link do GitHuba (tak ten bot jest opensource)"
            }
        ],
        timestamp: new Date(),
        footer: {
            icon_url: message.author.avatarURL,
            text: "Request komend helpa by " + message.author.username
        }
    }});}

    //========================================================================================
	
	if(command == "status") {
        switch (args[0]) {
            case "online":
                client.user.setStatus("online");
                message.channel.send("Status ustawiony na Online!");
                break;
            case "idle":
                client.user.setStatus("idle");
                message.channel.send("Status ustawiony na Zaraz Wracam (idle)!");
                break;
            case "dnd":
                client.user.setStatus("dnd");
                message.channel.send("Status ustawiony na Nie przeszkadzać (dnd)!");
                break;
            case "invisible":
                client.user.setStatus("invisible");
                message.channel.send("Status ustawiony na Niewidzialny!");
                break;
            default:
                message.channel.send("`online | idle | dnd | invisible`");
        }
	}

    //========================================================================================

    if (command == "ciastko")
    if (message.guild.member(message.author).voiceChannel) {
        message.guild.member(message.author).voiceChannel.join().then(connection => {
          const dispatcher = connection.playFile('ciastko.wav')
        })
        .catch(err => console.log(err));
    }

    //========================================================================================

    if(command == "przytul") {
        try{
            if (args[0] == null) {message.channel.send("Wzmień kogoś aby go przytulić!");} else {
                var pinged = message.mentions.members.first();
                message.channel.send("<@" + pinged.user.id + ">" + " dostałeś przytulasa od " + message.author.username + "!")
            }
        } catch(err) {}
    }

    //========================================================================================

    if(command == "zatancz") {
        message.channel.sendFile("taniec.gif")
    }

    //========================================================================================

    if(command == "google") {
        if (args[0] == null) {message.channel.send("Wpisz wrazę aby ją wyszukać!")} else {
            message.delete()
            message.channel.send("https://google.com/search?q=" + args.join(" ").replace(' ', '+') +  "&ie=UTF-8")
        }
    }

    //========================================================================================

    if(command == "popularnosc") {
      message.channel.send("Aktualnie jestem na " + client.guilds.size + " serwerach")
    }

    //========================================================================================

    if(command == "hardbass") {
      var hardbass = [
        "https://www.youtube.com/watch?v=3fEg1r6SH9Q",
        "https://www.youtube.com/watch?v=QiFBgtgUtfw",
        "https://www.youtube.com/watch?v=2elzr_SfqKc",
        "https://www.youtube.com/watch?v=AyDi8kI9gp0"
      ];
      var cokolwiek = Faker.random.arrayElement(hardbass)
      message.channel.send(cokolwiek)
    }

    //========================================================================================

    if(command == "strony") {
      message.channel.send({embed: {
        color: 0x20E888,
        author: {
          name: message.author.username,
          icon_url: message.author.avatarURL
        },
        title: "Lista fajnych stron WWW",
        fields: [{
          name: "Strony autorów bota",
          value: "https://orlow.com.pl" + ", " + "https://mrchest.tk\n" + "====="
        },
        {
          name: "Papryka (profesjonalna)",
          value: "https://papryka.pro\n" + "====="
        },
        {
          name: "Strona o informatyce",
          value: "https://kpvsky.pl"
        }
      ],
    }});
    }

    //========================================================================================

    if(command == "github") {
      message.channel.send("Repozytoria tego pięknego bota:\n" + "https://github.com/orfolke/oreo")
    }

    //========================================================================================

    if(command == "uzytkownik") {
        if(args[0] == null) {
            var gra = "Brak!";
            var stream = false;
            try{
                var gra = message.author.presence.game.name;
                stream = message.author.presence.game.streaming;
            } catch(err) {}
            message.channel.send({embed: {
                color: 0xf1c40f,
                author: {
                    name: message.author.tag,
                    icon_url: message.author.avatarURL
                },
                fields: [{
                    name: "Nick:",
                    value: message.author.username,
                    inline: true
                }, {
                    name: "Tag:",
                    value: message.author.discriminator,
                    inline: true
                }, {
                    name: "Gra:",
                    value: gra
                }, {
                    name:"Stream:",
                    value: stream,
                    inline: true
                }, {
                    name:"ID:",
                    value: message.author.id,
                    inline: true
                }, {
                    name:"Status:",
                    value: message.author.presence.status,
                    inline: true
                }],
                thumbnail: {
                    url: message.author.avatarURL
                }
            }});
        } else {
            message.guild.members.forEach(function(member) {
                if(`<@!${member.user.id}>` == args[0]) {
                    var gra = "Brak!";
                    var stream = false;
                    try{
                        var gra = member.presence.game.name;
                        stream = member.presence.game.streaming;
                    } catch(err) {}
                    message.channel.send({embed: {
                        color: 0xf1c40f,
                        author: {
                            name: member.user.tag,
                            icon_url: member.user.avatarURL
                        },
                        fields: [{
                            name: "Nick:",
                            value: member.user.username,
                            inline: true
                        }, {
                            name: "Tag:",
                            value: member.user.discriminator,
                            inline: true
                        }, {
                            name: "Gra:",
                            value: gra
                        }, {
                            name:"Stream:",
                            value: stream,
                            inline: true
                        }, {
                            name:"ID:",
                            value: member.user.id,
                            inline: true
                        }, {
                            name:"Status:",
                            value: member.presence.status,
                            inline: true
                        }],
                        thumbnail: {
                            url: member.user.avatarURL
                        }
                    }});
                }
            });
        }
    }
});

  //========================================================================================

client.login(config.token);